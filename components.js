Vue.component('card', {
  template: `
    <div class="card">
      <div class="card-name">
        {{ name || 'nada' }}
      </div>
      <div class="card-foot">
        {{ foot || 'nada' }}
      </div>
    </div>
  `,
  props: ['name','foot']
});
